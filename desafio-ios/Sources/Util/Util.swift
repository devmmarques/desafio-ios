//
//  Util.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 23/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import UIKit

class Util {
    
    func downloaderImage(url: String?) -> UIImage? {
        guard let url = url,
            let imageUrl = NSURL(string: url),
            let data = NSData(contentsOf: imageUrl as URL)
            else { return nil }
        
        return UIImage(data: data as Data)
    }
    
    
}
