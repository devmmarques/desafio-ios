//
//  Constant.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 22/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation


struct Constant {
    
    
    struct CellId {
    
        static let RepositoryCell = "repositoryCell"
        static let PullRequestCell = "pullRequestCell"
        static let HeaderCell = "headerCell"
    }
    
    struct ViewController {
        static let PullRequestIdentifier = "PullRequestViewController"
    }
    
    struct Error {
        static let RequestError = "Ocorreu um erro ao realizar a requisição"
    }
    
    struct Alert {
        static let Info = "Informação"
        static let Error = "Error"
        static let openSafari = "Será necessário abrir o Safari, deseja continuar ?"
        static let OK = "Ok"
        static let Cancel = "Cancelar"
        static let EmptyPullRequest = "Nenhum PullRequest encontrado."
    }
    
}
