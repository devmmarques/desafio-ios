//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    var title: String?
    var body: String?
    var dataCreate: String?
    var urlPull: String?
    
    var user: User?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        urlPull <- map["html_url"]
        dataCreate <- map["created_at"]
        user <- map["user"]
    }
    
}


class PullRequestViewModel {
    
    private var pullRequest: PullRequest
    
    init(pullRequest: PullRequest) {
        self.pullRequest = pullRequest
    }
    
    var title: String {
        return pullRequest.title ?? ""
    }
    
    var body: String {
        return pullRequest.body ?? ""
    }
    
    var urlPull: String {
        return pullRequest.urlPull ?? ""
    }
    
    
    var dataCreate: Date {
        return CSHelperDate.timestampFormatter.date(from: pullRequest.dataCreate!)!
    }
    
    var user: UserViewModel? {
        guard let user = pullRequest.user else {
            return nil
        }
        return UserViewModel(user: user)
    }
    
    
    
}
