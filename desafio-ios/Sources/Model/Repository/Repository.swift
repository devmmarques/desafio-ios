//
//  Repository.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 22/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var id: NSNumber?
    var name: String?
    var full_name: String?
    var dsscription: String?
    var forks_count: NSNumber?
    var stargazers_count: NSNumber?
    var pullsUrl: String?
    
    var owner: Owner?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        full_name <- map["full_name"]
        dsscription <- map["description"]
        forks_count <- map["forks_count"]
        stargazers_count <- map["stargazers_count"]
        pullsUrl <- map["pulls_url"]
        
        owner <- map["owner"]
    }
    
}

class RepositoryViewModel {
    
    private var repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
    }
    
    var id: NSNumber {
        return repository.id ?? 0
    }
    
    var name: String {
        return repository.name ?? ""
    }
    
    var fullName: String {
        return repository.full_name ?? ""
    }
    
    var description: String {
        return repository.dsscription ?? ""
    }
    
    var forkCount: NSNumber {
        return repository.forks_count ?? 0
    }
    
    var starCount: NSNumber {
        return repository.stargazers_count ?? 0
    }
    
    var pullsUrlString:String? {
        return repository.pullsUrl?.replacingOccurrences(of: "{/number}", with: "") ?? ""
    }
    
    var owner: OwnerViewModel? {
        guard let owner = repository.owner else {
            return nil
        }
        return OwnerViewModel(owner: owner)
    }
    
    
}
