//
//  Repositories.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import ObjectMapper

class Repositories: Mappable {
    
    var total_count: Int = 0
    var incomplete_results: Bool = false
    var items: [Repository]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        total_count <- map["total_count"]
        incomplete_results <- map["incomplete_results"]
        items <- map["items"]
    }
}
