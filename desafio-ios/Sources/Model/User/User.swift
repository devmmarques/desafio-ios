//
//  User.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    
    
    var id: NSNumber?
    var userName: String?
    var avatar: String?
  
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        userName <- map["login"]
        avatar <- map["avatar_url"]
    }
   
}




class UserViewModel {
    
    private var user: User
    
    init(user: User) {
        self.user = user
    }
    
    var id: NSNumber {
        return user.id ?? 0
    }
    
    var userName: String {
        return user.userName ?? ""
    }
  
    var avatar: String {
        return user.avatar ?? ""
    }
    
    
    
}

