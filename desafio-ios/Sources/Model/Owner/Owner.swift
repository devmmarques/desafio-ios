//
//  Owner.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 22/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    
    var id: NSNumber?
    var name: String?
    var login: String?
    var avatar_url: String?
    var url: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        login <- map["login"]
        avatar_url <- map["avatar_url"]
        url <- map["url"]
    }
    
}


class OwnerViewModel {
    private var owner: Owner
    
    init(owner: Owner) {
        self.owner = owner
    }
    
    var login:String {
        return owner.login ?? ""
    }
    
    var name:String {
        return owner.name ?? ""
    }
    
    var url:URL? {
        return URL(string: owner.url ?? "")
    }
    
    var avatarUrl:String {
        return owner.avatar_url ?? ""
    }
}

