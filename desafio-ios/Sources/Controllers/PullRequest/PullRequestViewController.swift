//
//  PullRequestViewController.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD


class PullRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PullReqRequestProtocol {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Properties
    var urlPullRequest: String?
    fileprivate var request = PullReqRequest()
    
    fileprivate var pullRequest: [PullRequest]?
    fileprivate var listPullRequest = [PullRequestViewModel]()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDelegation()
        self.getListPullRequest()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    
    // MARK: Methods
    
    fileprivate func setupDelegation() {
        self.request.delegate = self
    }
    
    fileprivate func getListPullRequest() {
        
        if let urlPull = urlPullRequest {
            KRProgressHUD.show()
            self.request.getPullRequest(with: URL(string: urlPull)!)
        }
    }
    
    func prepareItensViewCell() {
        
        guard let resultListPullRequest = pullRequest else {
            
            KRProgressHUD.showMessage(Constant.Alert.EmptyPullRequest)
            return
        }
        
        for pullReq in resultListPullRequest {
            self.listPullRequest.append(PullRequestViewModel(pullRequest: pullReq))
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPullRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellId.PullRequestCell, for: indexPath) as! PullRequestCell
        
        let pull = self.listPullRequest[indexPath.row]
        cell.fill(with: pull)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let actionPositive = UIAlertAction(title: Constant.Alert.OK, style: .default) { (action) in
            let pullRequestURL = self.listPullRequest[indexPath.item].urlPull
            UIApplication.shared.open(URL(string: pullRequestURL)!, options: [:], completionHandler: nil)
        }
        
        let actionNegative = UIAlertAction(title: Constant.Alert.Cancel, style: .cancel, handler: nil)
        
        self.showAlert(withTitle: Constant.Alert.Info, andMessage: Constant.Alert.openSafari, negativeAction: actionNegative, positiveAction: actionPositive)
        
    }
    
    
    // MARK: PullReqRequestProtocol
    
    func pullRequestDidFinish(pullRequest: [PullRequest]) {
        KRProgressHUD.dismiss()
        self.pullRequest = pullRequest
        self.prepareItensViewCell()
    }
    
    func pullRequestDidFinish(withError error: Error) {
        KRProgressHUD.dismiss()
        
        self.showAlert(withTitle: Constant.Alert.Error, andMessage: Constant.Error.RequestError, closeButtonTitle: Constant.Alert.OK)
    }
    
}
