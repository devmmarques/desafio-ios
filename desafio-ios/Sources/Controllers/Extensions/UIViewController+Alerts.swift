//
//  UIViewController+Alerts.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 25/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import UIKit

extension UIViewController {

    
    func showAlert(withTitle title: String? = nil, andMessage message: String? = nil,
                   negativeAction negative: UIAlertAction? = nil, positiveAction positive: UIAlertAction? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if negative != nil {
            alert.addAction(negative!)
        }
        if positive != nil {
            alert.addAction(positive!)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(withTitle title: String? = nil, andMessage message: String? = nil, closeButtonTitle closeTitle: String) {
        self.showAlert(withTitle: title, andMessage: message,
                       negativeAction: UIAlertAction(title: closeTitle, style: .cancel, handler: nil),
                       positiveAction: nil)
    }
    
    

}
