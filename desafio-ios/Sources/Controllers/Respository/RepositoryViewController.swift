//
//  RepositoryViewController.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

class RepositoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, RepositoryRequestProtocol {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Properties
    fileprivate var request = RepositoryRequest()
    fileprivate var repositoriesResult: Repositories?
    fileprivate var listItemsRepository = [RepositoryViewModel]()
    fileprivate var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDelegation()
        self.getListRepository()
    }
    
    
    // MARK: Methods
    fileprivate func setupDelegation() {
        self.request.delegate = self
    }
    
    fileprivate func confViewController() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    
    
    // MARK: Request
    
    fileprivate func getListRepository() {
        KRProgressHUD.show()
        
        if self.listItemsRepository.count < (repositoriesResult?.total_count ?? 0) {
            currentPage += 1
        }
        
        self.request.getRepository(page: currentPage)
    }
    
    
    fileprivate func prepareRepositoriesView() {
        guard let resultItens = repositoriesResult?.items else {
            // Criar o Alert
            return
        }
        
        for itens in resultItens {
            let itemRepository = RepositoryViewModel(repository: itens)
            self.listItemsRepository.append(itemRepository)
        }
        
        self.tableView.reloadData()
    }
    
    
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItemsRepository.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRepository = tableView.dequeueReusableCell(withIdentifier: Constant.CellId.RepositoryCell, for: indexPath) as! RepositoryCell
        
        let repository = listItemsRepository[indexPath.row]
            
        cellRepository.fill(with: repository)
        return cellRepository
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        
        let pullViewController = storyBoard.instantiateViewController(withIdentifier: Constant.ViewController.PullRequestIdentifier) as! PullRequestViewController
        
        let repository = self.listItemsRepository[indexPath.row]
        
        pullViewController.urlPullRequest = repository.pullsUrlString
        pullViewController.title = repository.name
        
        self.confViewController()
        
        self.navigationController?.pushViewController(pullViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = listItemsRepository.count - 1
        if indexPath.row == lastElement {
            
            if listItemsRepository.count < (repositoriesResult?.total_count ?? 0) {
                self.getListRepository()
            }
        }
    }
    
    
    // MARK: RepositoryRequestProtocol
    
    func repositoryRequestDidFinish(repositories: Repositories) {
        KRProgressHUD.dismiss()
        self.repositoriesResult = repositories
        self.prepareRepositoriesView()
    }
    
    
    func repositoryRequestDidFinish(withError error: Error) {
        KRProgressHUD.dismiss()
        KRProgressHUD.showMessage(Constant.Error.RequestError)
    }
    
    
}
