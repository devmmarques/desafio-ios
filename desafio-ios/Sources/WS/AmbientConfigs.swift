//
//  AmbientConfigs.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation


class AmbientConfigs {
    
    // MARK: URL API Base
    fileprivate static let baseUrlGit = "https://api.github.com/"
    
    // MARK: API paths
    
    fileprivate static let search = "search/repositories?q=language:Java&sort=stars&page=1"
    
    
//    fileprivate class func appendQuery(_ url: String) -> String {
//        let queryUrl = ""
//    }
    
    
    class func searchJavaRepository() -> String {
        return "\(baseUrlGit)\(search)"
    }
    
}
