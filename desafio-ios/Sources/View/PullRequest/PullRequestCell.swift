//
//  PullRequestCell.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import UIKit

class PullRequestCell: UITableViewCell {
    
    
    @IBOutlet weak var titlePullRequestLabel: UILabel!
    @IBOutlet weak var bodyPullRequestLabel: UILabel!
    @IBOutlet weak var avatarPullRequest: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var createAtLabel: UILabel!
    
    func fill(with pullRequest: PullRequestViewModel) {
        
        let util: Util = Util()
        
        self.titlePullRequestLabel.text = pullRequest.title
        self.bodyPullRequestLabel.text = pullRequest.body
        
        createAtLabel.text = CSHelperDate.formatterBR.string(from: pullRequest.dataCreate)
        
        guard let user = pullRequest.user else {
            return
        }
        userNameLabel.text = user.userName
        avatarPullRequest.image = util.downloaderImage(url: user.avatar)
        CSHelpers.makeRounded(imageView: avatarPullRequest)
        
    }
    
    
}
