//
//  RepositoryCell.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import UIKit
import FontAwesomeKit_Swift


class RepositoryCell: UITableViewCell, OwnerRequestProtocol {
    
    @IBOutlet weak var nameRepoLabel: UILabel!
    @IBOutlet weak var descriptionRepoLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var nameCompleteLabel: UILabel!
    
    @IBOutlet weak var numberForkLabel: UILabel!
    @IBOutlet weak var numberStarLabel: UILabel!
    
    var repository: RepositoryViewModel!
    fileprivate var requestOwner = RepositoryRequest()
    
    func fill(with repository: RepositoryViewModel) {
        self.repository = repository
        
        self.requestOwner.delegateOwner = self
        
        let util: Util = Util()
        
        self.getOwner(urlOwner: (self.repository.owner?.url)!)
        
        nameRepoLabel.text = repository.name.uppercased()
        descriptionRepoLabel.text = repository.description
        
        
        numberForkLabel.text = "\(repository.forkCount)"
        numberStarLabel.text = "\(repository.starCount)"

        guard let owner = repository.owner else {
            return
        }
        
        avatarImage.image = util.downloaderImage(url: owner.avatarUrl)

        CSHelpers.makeRounded(imageView: avatarImage)
        
    }
    
    func ownerRequestDidFinish(withError error: Error) {
        
    }
    
    func ownerRequestDidFinish(owner: Owner) {
        self.userNameLabel.text = owner.login ?? ""
        self.nameCompleteLabel.text = owner.name ?? ""
    }
    
    
    fileprivate func getOwner(urlOwner: URL) {
        self.requestOwner.getOwnerRepository(urlOwner: urlOwner)
    }
}
