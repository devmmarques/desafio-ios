//
//  PullReqRequest.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class PullReqRequest {
    
    // MARK: PullReqRequestProtocol
    var delegate: PullReqRequestProtocol?
    
    // MARK:
    func getPullRequest(with url: URL) {
        
        Alamofire.request(url).validate().responseArray { (response: DataResponse<[PullRequest]>) in
            switch response.result {
            case .success(let pullRequestResponse):
                self.delegate?.pullRequestDidFinish(pullRequest: pullRequestResponse)
                break
            case .failure(let error):
                self.delegate?.pullRequestDidFinish(withError: error)
                break
            }
        }
    }
    
    
}
