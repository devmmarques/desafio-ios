//
//  PullReqRequestProtocol.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 24/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation


protocol PullReqRequestProtocol: class {
    func pullRequestDidFinish(pullRequest: [PullRequest])
    func pullRequestDidFinish(withError error: Error)
    
}
