//
//  RepositoryRequest.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class RepositoryRequest {
    
    // MARK: Properties
    var delegate : RepositoryRequestProtocol?
    var delegateOwner : OwnerRequestProtocol?
    let url = AmbientConfigs.searchJavaRepository()
    
    
    // MARK: Request Repository
    func getRepository(page: Int) {
        
        let searchURL = "\(url)&page=\(page)"
        
        Alamofire.request(searchURL).validate().responseObject { (response: DataResponse<Repositories>) in
            switch response.result {
                case .success(let repositoriesResponse):
                    self.delegate?.repositoryRequestDidFinish(repositories: repositoriesResponse)
                    break
                case .failure(let error):
                    self.delegate?.repositoryRequestDidFinish(withError: error)
                    break
            }
        }
    }
    
    // MARK: Request Owner
    func getOwnerRepository(urlOwner: URL) {
        Alamofire.request(urlOwner).validate().responseObject { (response: DataResponse<Owner>) in
            switch response.result {
            case .success(let ownerResponse):
                self.delegateOwner?.ownerRequestDidFinish(owner: ownerResponse)
                break
            case .failure(let error):
                self.delegateOwner?.ownerRequestDidFinish(withError: error)
                break
            }
        }
    }
    
}
