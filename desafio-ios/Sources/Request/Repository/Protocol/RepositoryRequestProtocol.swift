//
//  RepositoryRequestProtocol.swift
//  desafio-ios
//
//  Created by Marco H M Marques on 21/09/17.
//  Copyright © 2017 Marco H M Marques. All rights reserved.
//

import Foundation


protocol RepositoryRequestProtocol: class {
    func repositoryRequestDidFinish(repositories: Repositories)
    func repositoryRequestDidFinish(withError error: Error)
}
